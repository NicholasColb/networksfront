A simple React application linked to a Node.js server for gathering data on how well people in a group know each other.

Preview: 

![picture](preview.png)
